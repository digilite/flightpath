		<footer itemscope itemtype="http://schema.org/WPFooter">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-6">
					  <?php wp_nav_menu([
						'theme_location' => 'footer-menu-1',
						'menu_class' => 'footer-menu footer-menu-1',
						'container' => '',
						]); ?>

					</div>
					<div class="col-md-3 col-6">
					    <?php wp_nav_menu([
						'theme_location' => 'footer-menu-2',
						'menu_class' => 'footer-menu footer-menu-2',
						'container' => '',
						]); ?>
					</div>
					<div class="col-md-3 col-6">
						<?php wp_nav_menu([
						'theme_location' => 'footer-menu-3',
						'menu_class' => 'footer-menu footer-menu-3',
						'container' => '',
						]); ?>
					</div>
				</div>
				<div class="row justify-content-between footer-logos">
					<div class="footer-social col-md-6">
						<?php get_template_part("social"); ?>
					</div>
					<div class="ft-logos flex-container justify-content-end col-md-6">
						<?php
						if( have_rows('footer_logos', 'options') ):
							while( have_rows('footer_logos',  'options') ) : the_row();
								$logo = get_sub_field('ft_logo');
								$size = 'full'; 
								if( $logo ) {
									echo wp_get_attachment_image( $logo, $size );
								}
								endwhile;
								else :
								endif; ?>
					</div>
				</div>
			</div>
			
			<div class="copy text-center">
				&copy; <?php echo date("Y") . " " . get_bloginfo("name"); ?>
			</div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>