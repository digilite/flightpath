<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'flightpath' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'h4?S=OzpC7Sa&x15np49JrjNcR[HT@R( Bad6=Q{ZrYc5Y*s<l4.sW#0^F>$f.[?' );
define( 'SECURE_AUTH_KEY',  'P%0u!7|`O-x&-L!Si=-((y{SjuG0 3z^5{(n0_|A($zSMjZR]P`l>EgEH=-;M>2F' );
define( 'LOGGED_IN_KEY',    '?:d+w%3}uVP.{G~@+/GR9}IXi&=P0,~:O@.b.9=I9uTF}vIgTJfOJL/JR:E7perh' );
define( 'NONCE_KEY',        '`Im~BRHLGC^-$TO!Z@5$ELYGu|}I.woo3+HkW>i|,ISrb$W/y]XN~Z_F)B3TU.NM' );
define( 'AUTH_SALT',        '|ZPtQ4TejIM{;J#3?r+#;f}.tN2M* o|Cd5Z=`8Vh2,[izi3~:SwM|_.yJ}-ujI(' );
define( 'SECURE_AUTH_SALT', '-j}ATkQGBf(PlnSUi@pDpETG]&9_u3++-=UXr~>N&g`-Q$oR8sbaNK1[-sW|zr1!' );
define( 'LOGGED_IN_SALT',   'aVi+ryo4cL(d|JRuSeoEF2&uMs=xp.fh5E/ L_P q8T6^pt=c5M~3t*L^R5_.=@2' );
define( 'NONCE_SALT',       '/}k3-178%,ut0jFNU!`-Cq3p(d1QFF`LE$vsTwlRT(SU`^XSXEB(nd$rI5`#o,Kt' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
