            var $ = jQuery;
            function new_map($el) {
                var $markers = $el.find(".marker"),
                    args = {
                        zoom: 24,
                        center: new google.maps.LatLng(0, 0),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        scrollwheel: !1,
                        disableDefaultUI: !0,
                        styles: [
                            { featureType: "water", elementType: "geometry", stylers: [{ color: "#e9e9e9" }, { lightness: 17 }] },
                            { featureType: "landscape", elementType: "geometry", stylers: [{ color: "#f5f5f5" }, { lightness: 20 }] },
                            { featureType: "road.highway", elementType: "geometry.fill", stylers: [{ color: "#ffffff" }, { lightness: 17 }] },
                            { featureType: "road.highway", elementType: "geometry.stroke", stylers: [{ color: "#ffffff" }, { lightness: 29 }, { weight: 0.2 }] },
                            { featureType: "road.arterial", elementType: "geometry", stylers: [{ color: "#ffffff" }, { lightness: 18 }] },
                            { featureType: "road.local", elementType: "geometry", stylers: [{ color: "#ffffff" }, { lightness: 16 }] },
                            { featureType: "poi", elementType: "geometry", stylers: [{ color: "#f5f5f5" }, { lightness: 21 }] },
                            { featureType: "poi.park", elementType: "geometry", stylers: [{ color: "#dedede" }, { lightness: 21 }] },
                            { elementType: "labels.text.stroke", stylers: [{ visibility: "on" }, { color: "#ffffff" }, { lightness: 16 }] },
                            { elementType: "labels.text.fill", stylers: [{ saturation: 36 }, { color: "#333333" }, { lightness: 40 }] },
                            { elementType: "labels.icon", stylers: [{ visibility: "off" }] },
                            { featureType: "transit", elementType: "geometry", stylers: [{ color: "#f2f2f2" }, { lightness: 19 }] },
                            { featureType: "administrative", elementType: "geometry.fill", stylers: [{ color: "#fefefe" }, { lightness: 20 }] },
                            { featureType: "administrative", elementType: "geometry.stroke", stylers: [{ color: "#fefefe" }, { lightness: 17 }, { weight: 1.2 }] },
                        ],
                    },
                    map = new google.maps.Map($el[0], args);
                return (
                    (map.markers = []),
                    $markers.each(function () {
                        add_marker($(this), map);
                    }),
                    center_map(map),
                    map
                );
            }
            function add_marker($marker, map) {
                var latlng = new google.maps.LatLng($marker.attr("data-lat"), $marker.attr("data-lng")),
                    marker = new google.maps.Marker({ position: latlng, map: map, icon: "https://dev.digilabs.ca/flightpath/wp-content/uploads/2022/04/plane.svg" });
                if ((map.markers.push(marker), $marker.html())) {
                    var infowindow = new google.maps.InfoWindow({ content: $marker.html() });
                    google.maps.event.addListener(marker, "click", function () {
                        infowindow.open(map, marker);
                    });
                }
            }
            function center_map(map) {
                var bounds = new google.maps.LatLngBounds();
                $.each(map.markers, function (i, marker) {
                    var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
                    bounds.extend(latlng);
                }),
                    1 == map.markers.length ? (map.setCenter(bounds.getCenter()), map.setZoom(14)) : map.fitBounds(bounds);
            }
            var map = null;
            $(document).ready(function () {
                $(".acf-map").each(function () {
                    map = new_map($(this));
                });
            }),
                $(document).ajaxComplete(function () {
                    $(".acf-map").each(function () {
                        map = new_map($(this));
                    });
                });

            function new_map($el) {
                var $markers = $el.find(".marker"),
                    $range = $el.find(".range").attr("data-range"),
                    args = {
                        zoom: mapInfo.zoom,
                        minZoom: 2,
                        center: new google.maps.LatLng(mapInfo.lat, mapInfo.lng),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        disableDoubleClickZoom: !0,
                        scrollwheel: !1,
                        disableDefaultUI: !1,
                        styles: [
                            { featureType: "water", elementType: "geometry", stylers: [{ color: "#e9e9e9" }, { lightness: 17 }] },
                            { featureType: "landscape", elementType: "geometry", stylers: [{ color: "#f5f5f5" }, { lightness: 20 }] },
                            { featureType: "road.highway", elementType: "geometry.fill", stylers: [{ color: "#ffffff" }, { lightness: 17 }] },
                            { featureType: "road.highway", elementType: "geometry.stroke", stylers: [{ color: "#ffffff" }, { lightness: 29 }, { weight: 0.2 }] },
                            { featureType: "road.arterial", elementType: "geometry", stylers: [{ color: "#ffffff" }, { lightness: 18 }] },
                            { featureType: "road.local", elementType: "geometry", stylers: [{ color: "#ffffff" }, { lightness: 16 }] },
                            { featureType: "poi", elementType: "geometry", stylers: [{ color: "#f5f5f5" }, { lightness: 21 }] },
                            { featureType: "poi.park", elementType: "geometry", stylers: [{ color: "#dedede" }, { lightness: 21 }] },
                            { elementType: "labels.text.stroke", stylers: [{ visibility: "on" }, { color: "#ffffff" }, { lightness: 16 }] },
                            { elementType: "labels.text.fill", stylers: [{ saturation: 36 }, { color: "#333333" }, { lightness: 40 }] },
                            { elementType: "labels.icon", stylers: [{ visibility: "off" }] },
                            { featureType: "transit", elementType: "geometry", stylers: [{ color: "#f2f2f2" }, { lightness: 19 }] },
                            { featureType: "administrative", elementType: "geometry.fill", stylers: [{ color: "#fefefe" }, { lightness: 20 }] },
                            { featureType: "administrative", elementType: "geometry.stroke", stylers: [{ color: "#fefefe" }, { lightness: 17 }, { weight: 1.2 }] },
                        ],
                    },
                    map = new google.maps.Map($el[0], args);
                return (
                    (map.markers = []),
                    (map.range = $range),
                    $markers.each(function () {
                        add_marker($(this), map);
                    }),
                    center_map(map),
                    create_range(map),
                    map
                );
            }
            function add_marker($marker, map) {
                var latlng = new google.maps.LatLng($marker.attr("data-lat"), $marker.attr("data-lng")),
                    marker = new google.maps.Marker({ position: latlng, map: map, icon: 'https://dev.digilabs.ca/flightpath/wp-content/uploads/2022/04/plane.svg' });
                if ((map.markers.push(marker), $marker.html())) {
                    var infowindow = new google.maps.InfoWindow({ content: $marker.html() });
                    google.maps.event.addListener(marker, "click", function () {
                        infowindow.open(map, marker);
                    });
                }
            }
            function center_map(map) {
                var bounds = new google.maps.LatLngBounds();
                $.each(map.markers, function (i, marker) {
                    var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
                    bounds.extend(latlng);
                });
            }
            function create_range(map) {
                function checkBounds(map) {
                    var newLat,
                        latNorth = map.getBounds().getNorthEast().lat(),
                        latSouth = map.getBounds().getSouthWest().lat();
                    if (
                        !(latNorth < 85 && latSouth > -85) &&
                        !(latNorth > 85 && latSouth < -85) &&
                        (latNorth > 85 && (newLat = map.getCenter().lat() - (latNorth - 85)), latSouth < -85 && (newLat = map.getCenter().lat() - (latSouth + 85)), newLat)
                    ) {
                        var newCenter = new google.maps.LatLng(newLat, map.getCenter().lng());
                        map.setCenter(newCenter);
                    }
                }
                var bounds = new google.maps.LatLngBounds();
                $.each(map.markers, function (i, marker) {
                    var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
                    bounds.extend(latlng);
                }),
                    console.log(map.range),
                    (map.radius = new google.maps.Circle({ strokeColor: "#ddd", strokeOpacity: 0.8, strokeWeight: 2, fillColor: "#000", fillOpacity: 0.35, map: map, center: bounds.getCenter(), radius: 1e3 * map.range, clickable: !1 })),
                    map.addListener("dblclick", function (e) {
                        console.log(e.latLng.lat() + "     " + e.latLng.lng()),
                            console.log(map.getBounds().contains({ lat: e.latLng.lat(), lng: e.latLng.lng() })),
                            map.getBounds().contains({ lat: e.latLng.lat(), lng: e.latLng.lng() }) && (console.log(e.latLng), placeMarker(e.latLng, map));
                    }),
                    google.maps.event.addListener(map, "center_changed", function () {
                        checkBounds(map);
                    });
            }
            function placeMarker(position, map) {
                for (var bounds = new google.maps.LatLngBounds(), i = 0; i < map.markers.length; i++) map.markers[i].setMap(null);
                map.markers = [];
                var marker = new google.maps.Marker({ position: position, icon: "https://dev.digilabs.ca/flightpath/wp-content/uploads/2022/04/plane.svg", map: map }),
                    latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
                bounds.extend(latlng),
                    map.radius.setMap(null),
                    (map.radius = new google.maps.Circle({ strokeColor: "#ddd", strokeOpacity: 0.8, strokeWeight: 2, fillColor: "#000", fillOpacity: 0.35, map: map, center: bounds.getCenter(), radius: 1e3 * map.range, clickable: !1 })),
                    (map.markers[0] = marker),
                    map.panTo(position);
            }
            var iconBase = "/wp-content/themes/flightpath/static/img/",
                map = null;
            $(document).ready(function () {
                $(".brand__map #map").each(function () {
                    map = new_map($(this));
                });
            });