<?php get_header(); 
/**
* Template Name: Company
*
* @package WordPress
* @subpackage Digilite Theme
*/
?>
<?php
    $hero_title = get_field('hero_title');
    $twc_desc = get_field('twc_description');
    $twc_image = get_field('twc_image');
    $size = 'full';
    $quote = get_field('cpm_quote');
    $m_title = get_field('mission_title');
    $m_desc = get_field('mission_description');
    $m_image = get_field('mission_image');
    $m_bg = get_field('mission_background');
    $ben_title = get_field('ben_title');
    $ben_bg = get_field("ben_background");
    $test_title = get_field('testi_title');
    $test_desc = get_field('testi_desc');
?>
<section class="home-hero company-hero" style="background: linear-gradient(180deg, #131E29 0%, rgba(19, 30, 41, 0.78) 29.85%, #131E29 62.27%), url('<?php echo get_the_post_thumbnail_url(); ?>') top center / cover no-repeat;">
    <div class="container">
        <p class="page-title bold-title"><?php the_title(); ?></p>
        <div class="row">
            <div class="hero-info col-md-7">
                <h1 class="general-title"><?= $hero_title; ?></h1>
            </div>
        </div>
        <div class="twc_image_text">
            <div class="row">
                <div class="col-md-6">
                    <div class="twc_ds">
                        <?= $twc_desc; ?>
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <?php echo wp_get_attachment_image( $twc_image, $size ); ?>
                </div>    
            </div>
        </div>    
    </div>
</section>
<section class="quote">
    <div class="container">
        <div class="quote-col">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="relative-parent">
                        <span class="quote-icon"></span>
                        <h2 class="quote-message"><?= $quote; ?></h2> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mission" style="background: url('<?= $m_bg; ?>') bottom left / contain no-repeat;">
    <div class="container">
            <div class="twc_image_text">
                <div class="row">
                    <div class="col-md-6">
                        <div class="twc_ds">
                            <h2 class="mission-title"><?= $m_title; ?></h2>
                            <p class="mission-desc"><?= $m_desc; ?></p>
                        </div>
                    </div>
                    <div class="col-md-6 mission-image">
                        <?php echo wp_get_attachment_image( $m_image, $size ); ?>
                    </div>    
                </div>
            </div> 
    </div>
</section>
<section class="corporate" id="corporate">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="benefits_title"><?= $ben_title; ?></h2>
            </div>
            <div class="col-md-6">
                <div class="accordion" id="accordionExample">
                    <?php
                    if( have_rows('ben_benefits_items') ):
                        $count = 1;
                        while( have_rows('ben_benefits_items') ) : the_row();
                            
                            $ben_item_title = get_sub_field('ben_item_title');
                            $ben_item_desc = get_sub_field('ben_item_description'); ?>
                            <div class="card">
                                <div class="card-header" id="heading<?= $count; ?>">
                                <h5 class="mb-0 acc-title">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?= $count; ?>" aria-expanded="<?php if($count == 1): echo "true"; endif; ?>" aria-controls="collapseOne">
                                    <?= $ben_item_title; ?>
                                    </button>
                                </h5>
                                </div>
                                <div id="collapse<?= $count; ?>" class="collapse <?php if($count == 1): echo "show"; endif; ?>" aria-labelledby="heading<?= $count; ?>" data-parent="#accordionExample">
                                <div class="card-body">
                                    <?= $ben_item_desc; ?>
                                </div>
                                </div>
                            </div>
                    <?php
                    $count ++;
                    endwhile;
                    else :
                    endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="testimonials">
    <div class="testimonial_title">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-12">
                    <h2 class="test-title"><?= $test_title; ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonial_desc">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-8">
                    <p class="test-desc"><?= $test_desc; ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonial-block">
        <div class="testimonial-carousel owl-carousel owl-theme">
            <?php
            if( have_rows('testimonial_items') ):
                while( have_rows('testimonial_items') ) : the_row();
                    $tname = get_sub_field('testi_name');
                    $tprof = get_sub_field('testi_profession');
                    $tdesc = get_sub_field('testi_rp_description'); ?>
                    <div class="item testimonial-item">
                    <p class="name bold-title"><?= $tname; ?></p>
                    <p class="prof"><?= $tprof; ?></p>
                    <p class="tdesc"><?= $tdesc; ?></p>
                    </div>
                <?php endwhile;
            else :
            endif; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>