<?php get_header(); 
/**
* Template Name: News & Stories
*
* @package WordPress
* @subpackage Digilite Theme
*/
?>
<section class="home-hero news-hero" style="background-image: linear-gradient(180deg, #131E29 0%, rgba(19, 30, 41, 0.78) 50%, #131E29 100%), url('<?php echo get_the_post_thumbnail_url(); ?>');">
    <div class="container">
        <div class="row">
            <div class="hero-info col-md-6">
                <h1 class="general-title"><?php the_title(); ?></h1>
                <div class="hero-info-description"><?php the_content(); ?></div>
            </div>
        </div>
    </div>
</section>
<section class="news-stories">
    <div class="container">
        <div id="news" class="tabcontent">
            <div class="row">
                <?php 
                    query_posts(array( 
                        'post_type' => 'post',
                        'showposts' => -1,
                        'cat' => 1
                    ) );  
                    ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <?php get_template_part("templates/card"); ?>
                <?php endwhile;?>
            </div>
        </div>
    </div>
</section>
<?php
    $bg1 = get_field('general_background', 19);
    $bg2 = get_field('second_background', 19);
    $ph_number = get_field('phone_number', 'option');
    $clear_phone = str_replace('-', '', $ph_number);
?>
<section class="call-to" style="background-image: url('<?= $bg1; ?>');">
    <div class="container">
        <div class="call-to-action text-center" style="background-image: url('<?= $bg2; ?>');">
            <?php $call_title = get_field('call_title', 19); ?>
            <h2 class="bold-title dark-title"><?= $call_title; ?></h2>
            <a  href="tel:<?= $clear_phone; ?>" class="fill-button-gold call-to-button"><?= $ph_number; ?></a>
        </div>
    </div>
</section>
<?php get_template_part("templates/request"); ?>
<?php get_footer(); ?>