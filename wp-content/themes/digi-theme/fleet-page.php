<?php get_header(); 
/**
* Template Name: Fleet
*
* @package WordPress
* @subpackage Digilite Theme
*/
?>
<?php
    $hero_title = get_field('hero_title');
    $hero_description = get_field('hero_description');
    $hero_subtitle = get_field('fleet_subtitle');
?>
<section class="home-hero" style="background-image: linear-gradient(180deg, #131E29 0%, rgba(19, 30, 41, 0.78) 50%, #131E29 100%), url('<?php echo get_the_post_thumbnail_url(); ?>');">
    <div class="container">
        <p class="page-title bold-title"><?php the_title(); ?></p>
        <div class="row">
            <div class="hero-info col-md-6">
                <h1 class="general-title"><?= $hero_title; ?></h1>
                <p class="subtitle bold-title"><?= $hero_subtitle; ?></p>  
                <p class="hero-info-description"><?= $hero_description; ?></p>
            </div>
        </div>  
    </div>
    <div class="fleet">
        <div class="fleet-types">
            <div class="tab">
                <?php 
                $terms = get_terms( array(
                    'taxonomy' => 'airplane_type',
                    'hide_empty' => false,
                ) );
                $count_tab = 1;
                foreach ($terms as $term){ ?>
                    <button class="tablinks bold-title tab_<?php echo $count_tab; ?>" onclick="openCity(event, '<?php echo $term->slug; ?>')"><?php echo $term->name; ?></button>       
                <?php 
                $count_tab ++;
                }
                ?>
            </div>
        </div>
    </div>
</section>
<section class="fleet-tab-content">
    <div class="container">
        <div id="light-jet" class="tabcontent">
            <div class="row">
                <div class="col-md-6">
                    <div class="term-info">
                        <?php $description = get_term_field( 'description', '7' ); ?>
                        <?php $title = get_term_field( 'name', '7' ); ?>
                        <p class="page-title fleet-title bold-title"><?= $title; ?></p>
                        <p class="fleet-desc"><?= $description; ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                query_posts(array( 
                    'post_type' => 'fleet',
                    'showposts' => 12,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'airplane_type',
                            'field' => 'slug',
                            'terms' => 'light-jet',
                        )
                    ),
                ) );  
                ?>
                <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part("templates/card"); ?>
                <?php endwhile;?>
            </div>
        </div>
        <div id="mid-size-jet" class="tabcontent">
            <div class="row">
                <div class="col-md-6">
                    <div class="term-info">
                        <?php $description = get_term_field( 'description', '8' ); ?>
                        <?php $title = get_term_field( 'name', '8' ); ?>
                        <p class="page-title fleet-title bold-title"><?= $title; ?></p>
                        <p class="fleet-desc"><?= $description; ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                query_posts(array( 
                    'post_type' => 'fleet',
                    'showposts' => 12,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'airplane_type',
                            'field' => 'slug',
                            'terms' => 'mid-size-jet',
                        )
                    ),
                ) );  
                ?>
                <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part("templates/card"); ?>
                <?php endwhile;?>
            </div>
        </div>
        <div id="heavy-jet" class="tabcontent">
            <div class="row">
                <div class="col-md-6">
                    <div class="term-info">
                        <?php $description = get_term_field( 'description', '9' ); ?>
                        <?php $title = get_term_field( 'name', '9' ); ?>
                        <p class="page-title fleet-title bold-title"><?= $title; ?></p>
                        <p class="fleet-desc"><?= $description; ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                query_posts(array( 
                    'post_type' => 'fleet',
                    'showposts' => 12,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'airplane_type',
                            'field' => 'slug',
                            'terms' => 'heavy-jet',
                        )
                    ),
                ) );  
                ?>
                <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part("templates/card"); ?>
                <?php endwhile;?>
            </div>
        </div>
        <div id="turbo-prop" class="tabcontent">
            <div class="row">
                <div class="col-md-6">
                    <div class="term-info">
                        <?php $description = get_term_field( 'description', '10' ); ?>
                        <?php $title = get_term_field( 'name', '10' ); ?>
                        <p class="page-title fleet-title bold-title"><?= $title; ?></p>
                        <p class="fleet-desc"><?= $description; ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                query_posts(array( 
                    'post_type' => 'fleet',
                    'showposts' => 12,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'airplane_type',
                            'field' => 'slug',
                            'terms' => 'turbo-prop',
                        )
                    ),
                ) );  
                ?>
                <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part("templates/card"); ?>
                <?php endwhile;?>
            </div>
        </div>
    </div>
</section>
<?php get_template_part("templates/request"); ?>
<?php get_footer(); ?>