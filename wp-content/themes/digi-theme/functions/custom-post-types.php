<?php
// ADD CUSTOM POST TYPE IF NEEDED

function custom_post_type_events() {
	$labels = [
		"name"               => _x("Services", "Services"),
		"singular_name"      => _x("Services", "services"),
		"add_new"            => _x("Add New", "Service"),
		"add_new_item"       => __("Add New Service"),
		"edit_item"          => __("Edit Service"),
		"new_item"           => __("New Service"),
		"all_items"          => __("All Services"),
		"view_item"          => __("View Services"),
		"search_items"       => __("Search Services"),
		"not_found"          => __("No Services found"),
		"not_found_in_trash" => __("No Services found in the Trash"), 
		"parent_item_colon"  => "",
		"menu_name"          => "Services"
	];

	$args = [
		"labels"        => $labels,
		"description"   => "Holds our Services and Service specific data",
		"public"        => true,
		"menu_position" => 5,
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields", "post-templates"],
		"has_archive"   => true,
	];
	register_post_type("services", $args);
}
add_action("init", "custom_post_type_events");





function custom_post_type_fleet() {
	$labels = [
		"name"               => _x("Fleet", "Fleet"),
		"singular_name"      => _x("Fleet", "fleet"),
		"add_new"            => _x("Add New", "Airplane"),
		"add_new_item"       => __("Add New Airplane"),
		"edit_item"          => __("Edit Airplane"),
		"new_item"           => __("New Airplane"),
		"all_items"          => __("Fleet"),
		"view_item"          => __("View Fleet"),
		"search_items"       => __("Search Airplane"),
		"not_found"          => __("No airplanes found"),
		"not_found_in_trash" => __("No airplanes found in the Trash"), 
		"parent_item_colon"  => "",
		"menu_name"          => "Fleet"
	];

	$args = [
		"labels"        => $labels,
		"description"   => "Holds our Fllet and Airplane specific data",
		"public"        => true,
		"menu_position" => 5,
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields"],
		"has_archive"   => true,
	];
	register_post_type("fleet", $args);
}
add_action("init", "custom_post_type_fleet");
