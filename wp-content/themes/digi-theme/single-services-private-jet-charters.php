<?php
/**
 * Template Name: Services Private Jet Charter
 * Template Post Type: services
 */
 get_header();
    $hero_title = get_field('hero_title');
    $hero_image = get_field('hero_image');
    $two_image_left = get_field('left_image');
    $two_image_right = get_field('right_image');
    $size = 'full';
    $jet_service_title = get_field('jet_services_title');
    $jet_service_description = get_field('jet_services_description');
?>
<section class="home-hero pppp" style="background: linear-gradient(180deg, #131E29 0%, rgba(19, 30, 41, 0.78) 50%, #131E29 100%), url('<?php echo esc_url($hero_image['url']); ?>') top center / cover no-repeat;">
    <div class="container">
        <p class="page-title bold-title"><?php the_title(); ?></p>
        <div class="row">
            <div class="hero-info col-md-7">
                <h1 class="general-title"><?= $hero_title; ?></h1>
            </div>
        </div>
    </div>
</section>
<section class="jet-images-text">
    <div class="jet-text-blocks">
            <?php
            if( have_rows('text_blocks') ):
                $count = 1;
                while( have_rows('text_blocks') ) : the_row();
                    $pr_desc = get_sub_field('jet_tx_description');
                    $pr_image = get_sub_field('jet_tx_image');
                    $size = 'full';?>
                    <div class="flex-container purchasing-item">
                        <div class="half-width <?php if($count % 2 === 0): echo 'pr-info pr-bg-left'; else: echo "pr-image"; endif; ?>">
                            <?php if($count % 2 === 0): ?>
                            <div class="pr-title-desc">
                                <?= $pr_desc; ?>
                            </div>
                            <?php else:
                            echo wp_get_attachment_image( $pr_image, $size ); ?>
                            <?php endif; ?>
                        </div>
                        <div class="half-width <?php if($count % 2 === 0): echo 'pr-image'; else: echo "pr-info pr-bg-right"; endif; ?>">
                            <?php if($count % 2 === 0):
                            echo wp_get_attachment_image( $pr_image, $size );
                            else: ?>
                            <div class="pr-title-desc">
                                <?= $pr_desc; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php $count ++; ?>
                <?php endwhile;
            else :
            endif; ?>
    </div>
</section>
<section class="private-jet-servies relative-parent"> 
    <div class="white-section">
        <div class="container">
            <div class="row jusify-content-start">
                <div class="col-md-4">
                    <h2 class="jet-service-title"><?= $jet_service_title; ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="blue-section">
        <div class="container">
            <div class="row jusify-content-start">
                <div class="col-md-4">
                    <p class="jet-service-desc"><?= $jet_service_description; ?></p>    
                </div>
            </div>
        </div>
    </div>
    <div class="jet-services-blocks"1>
        <div class="container">
            <div class="mobile-jet">
                <h2 class="jet-service-title"><?= $jet_service_title; ?></h2>
                <p class="jet-service-desc"><?= $jet_service_description; ?></p>
            </div>
            <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <?php
                        if( have_rows('jet_services') ):
                            while( have_rows('jet_services') ) : the_row();
                                $jet_s_title = get_sub_field('jet_s_title');
                                $jet_s_description = get_sub_field('jet_s_description');
                                $jet_s_icon = get_sub_field('jet_s_icon'); ?>
                                <div class="col-md-6 col-12 d-flex">
                                    <div class="jet-s-item">
                                        <div class="jet-icon">
                                            <?php echo wp_get_attachment_image( $jet_s_icon, $size ); ?>
                                        </div>
                                        <h4 class="jet-s-title bold-title"><?= $jet_s_title; ?></h4>
                                        <p class="jet-s-description"><?= $jet_s_description; ?></p>
                                    </div>  
                                </div>
                            <?php endwhile;
                        else :
                        endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    $bg1 = get_field('general_background', 19);
    $bg2 = get_field('second_background', 19);
    $ph_number = get_field('phone_number', 'option');
    $clear_phone = str_replace('-', '', $ph_number);
?>
<section class="call-to" style="background-image: url('<?= $bg1; ?>');">
    <div class="container">
        <div class="call-to-action text-center" style="background-image: url('<?= $bg2; ?>');">
            <?php $call_title = get_field('call_title', 19); ?>
            <h2 class="bold-title dark-title"><?= $call_title; ?></h2>
            <a  href="tel:<?= $clear_phone; ?>" class="fill-button-gold call-to-button"><?= $ph_number; ?></a>
        </div>
    </div>
</section>
<?php get_footer(); ?>