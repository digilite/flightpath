<?php
/**
 * Template Name: Single Fleet
 */
 get_header();
    $hero_short_desc = get_field('short_description');
    $hero_image = get_field('hero_image');
    $related_background = get_field('related_block_background', 'option');
    $location = get_field("point_of_origin");
    $terms = wp_get_post_terms(get_the_ID(), "airplane_type");
?>
<script>
    // Exports for brand-map.js
    var mapInfo = {
        lat: <?php echo ($terms[0]->name != 'Heavy Jet') ? $location['lat'] : '0';?>,
        lng: <?php echo ($terms[0]->name != 'Heavy Jet') ? $location['lng'] : '0';?>,
        zoom: <?php echo ($terms[0]->name != 'Heavy Jet') ? '3' : '2';?>,

    }
</script>
<section class="home-hero" style="background: linear-gradient(180deg, #131E29 0%, rgba(19, 30, 41, 0.78) 50%, #131E29 100%), url('<?php the_post_thumbnail_url('full'); ?>') top center / cover no-repeat;">
    <div class="container">
        <p class="page-title bold-title">Our Fleet</p>
        <div class="row">
            <div class="hero-info col-md-6">
                <h1 class="general-title"><?php the_title(); ?></h1>
                <p class="hero-info-description"><?= $hero_short_desc; ?></p>
            </div>
        </div>
        <div class="specifications">
            <div class="row">
                <div class="col-md-6">
                    <div class="specification-content">
                        <?php the_content(); ?>
                    </div>
                    <?php 
                        $images = get_field('fleet_gallery');
                        if( $images ): ?>
                            <div id="gallery" class="row">
                                <?php foreach( $images as $image ): ?>
                                        <div class="col-lg-4">
                                            <div class="lb-item">
                                                <a data-toggle="lightbox" href="<?php echo $image['url']; ?>"><img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
                                            </div>
                                        </div>  
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                </div>
                <div class="col-md-6">
                    <div class="secifications-items">
                        <?php
                        if( have_rows('specifications') ):
                            while( have_rows('specifications') ) : the_row();
                                $label = get_sub_field('spec_label');
                                $value = get_sub_field('spec_value'); ?>
                                <div class="spec-item">
                                    <p class="spec-label"><?= $label; ?></p>
                                    <p class="spec-value bold-title text-right"><?= $value; ?></p>
                                </div>
                            <?php endwhile;
                        else :
                        endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="fleet-map">
    <div class="brand__map">
        <div class="" id="map"> 
            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
            <div class="range" data-range="<?php the_field("range_of_aircraft_in_km"); ?>"></div>
        </div>
    </div>
</section>
<section class="related" style="background: url('<?php echo esc_url($related_background['url']); ?>') center center / cover no-repeat;">
    <div class="container">
        <p class="page-title bold-title">Our Fleet</p>
        <div class="row">
        <?php 
            $terms = get_the_terms( get_the_ID(), 'airplane_type' );
            if ( !empty( $terms ) ){
                $term = array_shift( $terms );
            }
            query_posts(array( 
                    'post_type' => 'fleet',
                    'showposts' => 4,
                    'post__not_in' => array(get_the_ID()),
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'airplane_type',
                            'field' => 'slug',
                            'terms' => $term->slug,
                        )
                    ),
                ) );  
                ?>
                <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part("templates/card"); ?>
            <?php endwhile;?>
        </div>
    </div>
</section>
<?php get_footer(); ?>