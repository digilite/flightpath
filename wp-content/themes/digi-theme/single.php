<?php get_header();
$hero_image = get_field('single_hero_image', 'option');
?>
<section class="home-hero news-hero" style="background-image: linear-gradient(180deg, #131E29 0%, rgba(19, 30, 41, 0.78) 50%, #131E29 100%), url('<?= $hero_image; ?>');">
    <div class="container"> 
        <div class="row">
            <div class="hero-info col-md-6">
				<p class="page-title bold-title"><?php echo get_the_date(); ?></p>
                <h1 class="general-title"><?= the_title(); ?></h1>
            </div>
			<div class="col-md-6">
				<div class="single-thumb" style="background: url('<?php the_post_thumbnail_url(); ?>') center center / cover no-repeat">
				</div>
			</div>
        </div>
    </div>
</section>
<section class="single-content">
	<div class="container">
		<div class="user-content">
			<?php
			if (have_posts()) :
				while (have_posts()) :
					the_post(); ?>
					<?php the_content(); ?><?php
				endwhile;
			endif; ?>
		</div>
	</div>
</section>
<div class="related">
	<div class="container">
		<h3 class="related-title">MORE STORIES LIKE THIS</h3>
		<div class="row">
			<?php
			$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 4, 'post__not_in' => array($post->ID) ) );
			if( $related ) foreach( $related as $post ) {
			setup_postdata($post); ?>
			<?php get_template_part("templates/card"); ?>   
			<?php }
			wp_reset_postdata(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>