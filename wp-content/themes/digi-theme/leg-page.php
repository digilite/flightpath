<?php get_header(); 
/**
* Template Name: Empty Leg
*
* @package WordPress
* @subpackage Digilite Theme
*/
?>
<?php
    $hero_title = get_field('hero_title');
    $hero_description = get_field('hero_description');
    $hero_subtitle = get_field('fleet_subtitle');
    $hero_right_desc = get_field('leg_right_description');
    $g_bg = get_field('g_bg');
?>
<section class="home-hero" style="background-image: linear-gradient(180deg, #131E29 0%, rgba(19, 30, 41, 0.78) 50%, #131E29 100%), url('<?php echo get_the_post_thumbnail_url(); ?>');">
    <div class="container">
        <p class="page-title bold-title"><?php the_title(); ?></p>
        <div class="row">
            <div class="hero-info col-md-6">
                <h1 class="general-title"><?= $hero_title; ?></h1>
                <p class="subtitle bold-title"><?= $hero_subtitle; ?></p>  
                <p class="hero-info-description"><?= $hero_description; ?></p>
            </div>
            <div class="col-md-6 right_desc">
                <p class="hero-info-description"><?= $hero_right_desc; ?></p>
            </div>
        </div>  
    </div>
</section>
<section class="booking" style="background: url('<?= $g_bg; ?>') center center / cover no-repeat;">
    <div class="container">
        <div id="fyEQAFrameWrapper" class="fyEQAFrameWrapper">
            <style type="text/css" scoped>.fyEQAFrameWrapper .fyEQAFrame {width: 100% !important;margin:auto;}</style>
            <iframe id="fyEQAFrame" class="fyEQAFrame" src="https://flyeasy.co/opapi/-fe3-dt1-ctp:flyflightpath:-/54b7e593e636e96f3af16ce1/" style="max-width: 100%; margin-right: auto; margin-left: auto; height: 1500px; border: none; min-height: 460px;"></iframe>
        </div>
        <script src="https://flyeasy.co/js/loaders/all.api.loader.min.js"></script><script>initEQA({autosize:
                            true, gaCrossDomainTracking: 'UA-102568840-2'});</script>
    </div>
</section>
<?php get_template_part("templates/request"); ?>
<?php get_footer(); ?>