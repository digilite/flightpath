var $ = jQuery;
var single_slide_bullets_fade = {
	loop: true,
	dots: false,
    autoplay: true,
    nav:true,
    lazyLoad: true,
    margin:24,
    navText: ['<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512"><path d="M216.4 163.7c5.1 5 5.1 13.3.1 18.4L155.8 243h231.3c7.1 0 12.9 5.8 12.9 13s-5.8 13-12.9 13H155.8l60.8 60.9c5 5.1 4.9 13.3-.1 18.4-5.1 5-13.2 5-18.3-.1l-82.4-83c-1.1-1.2-2-2.5-2.7-4.1-.7-1.6-1-3.3-1-5 0-3.4 1.3-6.6 3.7-9.1l82.4-83c4.9-5.2 13.1-5.3 18.2-.3z" /><rect x="0" y="0" width="512" height="512" fill="rgba(0, 0, 0, 0)" /></svg>Previous','Next<svg viewBox="0 0 512 512"><path d="M322.2 349.7c-3.1-3.1-3-8 0-11.3l66.4-74.4H104c-4.4 0-8-3.6-8-8s3.6-8 8-8h284.6l-66.3-74.4c-2.9-3.4-3.2-8.1-.1-11.2 3.1-3.1 8.5-3.3 11.4-.1 0 0 79.2 87 80 88s2.4 2.8 2.4 5.7-1.6 4.9-2.4 5.7-80 88-80 88c-1.5 1.5-3.6 2.3-5.7 2.3s-4.1-.8-5.7-2.3z" /><rect x="0" y="0" width="512" height="512" fill="rgba(0, 0, 0, 0)" /></svg>'],
	autoplayTimeout: 7000,
	responsiveClass: true,
    responsive:{
        0:{
            items:1,
            autoHeight:true,
        },
        992:{
            items:2,
        },
    }
};

var single_slide_bullets = {
	loop: true,
    autoplay: true,
    animateIn: "fadeIn",
	animateOut: "fadeOut", 
	autoplayTimeout: 7000,
    items:1,
	responsiveClass: true,
    responsive:{
        0:{
            dots: false,
            autoHeight:true,
        },
        768:{
            dots: true,
        },
    }
};


var slider_section = $('.slider-section');
var service_slider = $(".service-slider");

$(document).ready(function() {

    $('.view-book').on('change', function() {
        var show_content = this.value;
        $('.frm_fly').css('display', 'none');
        $('.' + show_content).css('display', 'block');
    })

    $('.fleet-types .tab_1').addClass('active');
    


    $("#burger-icon").click(function(){
        $(this).toggleClass("open");
        $(".mobile-menu").toggleClass("visible");
        $("body").toggleClass('open-menu');
    });

    if(slider_section.length) {
		slider_section.owlCarousel(single_slide_bullets);
		slider_section.find(".owl-controls .owl-dots").wrap("<div class='container'></div>")
	}

	if(service_slider.length) {
		service_slider.owlCarousel(full_gutter_24);
    }
    $(".user-content iframe").each(function(){
        $(this).wrap("<div class='embed-responsive embed-responsive-16by9'></div>")
    });
  
    
    $(".move-down").click(function(){
        var hero_height = $(this).parent().parent().height();
        var to_scroll = hero_height + $("header").height() + 34;
        $("html, body").animate({ scrollTop:  to_scroll }, 2000);
    });
    if ($(".grid").length) {
        $(".grid").packery({
            // options
            itemSelector: ".grid-item",
        });
    }



    $('.main-carousel').owlCarousel({
        margin:24,
        nav:true,
        responsiveClass:true,
        navText: ['<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.66536 16.0002C2.66536 8.63637 8.6349 2.66683 15.9987 2.66683C23.3625 2.66683 29.332 8.63637 29.332 16.0002C29.332 23.364 23.3625 29.3335 15.9987 29.3335C8.6349 29.3335 2.66536 23.364 2.66536 16.0002Z" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M16 21.3335L10.6667 16.0002L16 10.6668" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M21.332 16H10.6654" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>'
    ,'<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M29.3345 15.9996C29.3345 23.3635 23.3649 29.333 16.0011 29.333C8.63733 29.333 2.66783 23.3635 2.66783 15.9996C2.66783 8.63584 8.63734 2.66634 16.0011 2.66634C23.3649 2.66634 29.3345 8.63584 29.3345 15.9996Z" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M16.0002 10.6663L21.3335 15.9996L16.0002 21.333" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M10.6679 16L21.3345 16" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>'],
        responsive:{
            0: {
                items: 1,
                loop: true
            },
    
            560: {
                items: 3,
                loop: true,
                center:true,
                animateOut: 'fadeOut'
            },
    
            991: {
                items: 3,
                loop: true,
                center:true,
                animateOut: 'fadeOut'
            }
        }
    })

    $('.testimonial-carousel').owlCarousel({
        loop:true,
        animateOut: 'fadeOut',
        margin:24,
        nav:true,
        navText: ['<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.66536 16.0002C2.66536 8.63637 8.6349 2.66683 15.9987 2.66683C23.3625 2.66683 29.332 8.63637 29.332 16.0002C29.332 23.364 23.3625 29.3335 15.9987 29.3335C8.6349 29.3335 2.66536 23.364 2.66536 16.0002Z" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M16 21.3335L10.6667 16.0002L16 10.6668" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M21.332 16H10.6654" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>'
    ,'<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M29.3345 15.9996C29.3345 23.3635 23.3649 29.333 16.0011 29.333C8.63733 29.333 2.66783 23.3635 2.66783 15.9996C2.66783 8.63584 8.63734 2.66634 16.0011 2.66634C23.3649 2.66634 29.3345 8.63584 29.3345 15.9996Z" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M16.0002 10.6663L21.3335 15.9996L16.0002 21.333" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M10.6679 16L21.3345 16" stroke="#75787B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>'],
        responsive:{
            0: {
                items: 1,
                loop: true
            },
    
            560: {
                items: 1,
                loop: true
            },
    
            991: {
                items: 2,
                loop: true,
                autoWidth:true
            },
        }
    })
  

    
});

$( ".mobile-menu .menu-item-has-children" ).one("click",function(e) {
    e.preventDefault();
    $(this).find(".sub-menu").toggle( "fast" );
});


function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;
  
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
  
    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks_blog");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
  
    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }


  (function( $ ) {

    /**
     * initMap
     *
     * Renders a Google Map onto the selected jQuery element
     *
     * @date    22/10/19
     * @since   5.8.6
     *
     * @param   jQuery $el The jQuery element.
     * @return  object The map instance.
     */
    function initMap( $el ) {
    
        // Find marker elements within map.
        var $markers = $el.find('.marker');
    
        // Create gerenic map.
        var mapArgs = {
            zoom        : $el.data('zoom') || 16,
            mapTypeId   : google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map( $el[0], mapArgs );
    
        // Add markers.
        map.markers = [];
        $markers.each(function(){
            initMarker( $(this), map );
        });
    
        // Center map based on markers.
        centerMap( map );
    
        // Return map instance.
        return map;
    }
    
    /**
     * initMarker
     *
     * Creates a marker for the given jQuery element and map.
     *
     * @date    22/10/19
     * @since   5.8.6
     *
     * @param   jQuery $el The jQuery element.
     * @param   object The map instance.
     * @return  object The marker instance.
     */
    function initMarker( $marker, map ) {
    
        // Get position from marker.
        var lat = $marker.data('lat');
        var lng = $marker.data('lng');
        var latLng = {
            lat: parseFloat( lat ),
            lng: parseFloat( lng )
        };
    
        // Create marker instance.
        var marker = new google.maps.Marker({
            position : latLng,
            map: map
        });
    
        // Append to reference for later use.
        map.markers.push( marker );
    
        // If marker contains HTML, add it to an infoWindow.
        if( $marker.html() ){
    
            // Create info window.
            var infowindow = new google.maps.InfoWindow({
                content: $marker.html()
            });
    
            // Show info window when marker is clicked.
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open( map, marker );
            });
        }
    }
    
    /**
     * centerMap
     *
     * Centers the map showing all markers in view.
     *
     * @date    22/10/19
     * @since   5.8.6
     *
     * @param   object The map instance.
     * @return  void
     */
    function centerMap( map ) {
    
        // Create map boundaries from all map markers.
        var bounds = new google.maps.LatLngBounds();
        map.markers.forEach(function( marker ){
            bounds.extend({
                lat: marker.position.lat(),
                lng: marker.position.lng()
            });
        });
    
        // Case: Single marker.
        if( map.markers.length == 1 ){
            map.setCenter( bounds.getCenter() );
    
        // Case: Multiple markers.
        } else{
            map.fitBounds( bounds );
        }
    }
    
    // Render maps on page load.
    $(document).ready(function(){
        $('.acf-map').each(function(){
            var map = initMap( $(this) );
        });
    });

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            maxWidth: '80%'
        });
    });
    
    })(jQuery);