<?php get_header(); 
/**
* Template Name: Homepage
*
* @package WordPress
* @subpackage Digilite Theme
*/
?>
<?php
    $hero_title = get_field('hero_title');
    $hero_description = get_field('hero_description');
    $hero_bt_label = get_field('hero_button_label');
    $hero_bt_url = get_field('hero_button_url');
    $link_url = $hero_bt_url['url'];
?>
<section class="home-hero" style="background: linear-gradient(180deg, #131E29 0%, rgba(19, 30, 41, 0.78) 50%, #131E29 100%), url('<?php echo get_the_post_thumbnail_url(); ?>') center center / cover no-repeat;">
    <div class="container">
        <div class="row">
            <div class="hero-info col-md-6 col-12">
                <h1 class="general-title"><?= $hero_title; ?></h1>
                <p class="hero-info-description"><?= $hero_description; ?></p>
                <a href="<?php echo esc_url( $link_url ); ?>" class="fill-button-gold"><?= $hero_bt_label; ?></a>
            </div>
        </div>
    </div>
    <div class="services-carousel col-md-12">
        <div class="owl-carousel owl-theme main-carousel">
        <?php
            $featured_posts = get_field('home_carousel');
            if( $featured_posts ): ?>
                <?php foreach( $featured_posts as $post ): 

                    // Setup this post for WP functions (variable must be named $post).
                    setup_postdata($post); ?>
                    <div class="item relative-parent">
                        <?php the_post_thumbnail( 'full' ); ?>
                        <div class="service-info">
                            <p class="service_subtitle"><?php the_field('services_subtitle'); ?></p>
                            <a class="service_title bold-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>  
                    </div>
                <?php endforeach; ?>
                <?php 
                // Reset the global post object so that the rest of the page works correctly.
                wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php get_template_part("templates/request"); ?>
<?php 
    $s_image = get_field('s_background');
?>
<section class="services flex-container">
     <?php 
        $s_image = get_field('s_background');
     ?>
    <div class="col-md-6 col-12 services-back" style="background: url('<?= $s_image; ?>') center center / cover no-repeat;">   
    </div>
    <div class="col-md-6 services-list">
    <?php
    $count = 1;
    if( have_rows('s_list') ):
        while( have_rows('s_list') ) : the_row();
            $icon = get_sub_field('s_icon');
            $s_size = 'full';
            $title = get_sub_field('s_title');
            $description = get_sub_field('s_description');
            $bt_label = get_sub_field('s_button_label');
            $bt_url = get_sub_field('s_button_url');
            ?>
            <div class="service-item s-<?= $count; ?> flex-container">
                <div class="col-md-2 col-3">
                    <?php if( $icon ) {
                        echo wp_get_attachment_image( $icon, $s_size );
                    }?>
                </div>
                <div class="col-md-8 col-9">
                    <h3 class="s-title"><?= $title; ?></h3>
                    <p class="s-description"><?= $description; ?></p>
                    <a class="button-gold learn-more" href="<?= $bt_url; ?>"><?= $bt_label; ?></a>
                </div>
            </div>   
        <?php
        $count++;
        endwhile;
        else :
        endif; ?>
    </div>
</section>
<?php
    $bg1 = get_field('general_background');
    $bg2 = get_field('second_background');
    $ph_number = get_field('phone_number', 'option');
    $clear_phone = str_replace('-', '', $ph_number);
?>
<section class="call-to" style="background-image: url('<?= $bg1; ?>');">
    <div class="container">
        <div class="call-to-action text-center" style="background-image: url('<?= $bg2; ?>');">
            <?php $call_title = get_field('call_title'); ?>
            <h2 class="bold-title dark-title"><?= $call_title; ?></h2>
            <a  href="tel:<?= $clear_phone; ?>" class="fill-button-gold call-to-button"><?= $ph_number; ?></a>
        </div>
    </div>
</section>
<?php get_footer(); ?>