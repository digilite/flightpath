<?php get_header(); 
/**
* Template Name: Safety
*
* @package WordPress
* @subpackage Digilite Theme
*/
?>
<?php
    $hero_title = get_field('hero_title');
    $hero_subtitle = get_field('hero_subtitle');
    $twc_desc = get_field('twc_description');
    $twc_image = get_field('twc_image');
    $size = 'full';
?>
<section class="home-hero" style="background: linear-gradient(180deg, #131E29 0%, rgba(19, 30, 41, 0.78) 50%, #131E29 100%), url('<?php echo get_the_post_thumbnail_url(); ?>') top center / cover no-repeat;">
    <div class="container">
        <p class="page-title bold-title"><?= $hero_subtitle; ?></p> 
        <div class="row">
            <div class="hero-info col-md-5">
                <h1 class="general-title"><?= $hero_title; ?></h1>
            </div>
        </div>   
    </div>
</section>
<section class="sft_text_with_image">
    <div class="container">
        <div class="twc_image_text">
            <dic class="row">
                <div class="col-md-6 twc_ds">
                    <?= $twc_desc; ?>
                </div>
                <div class="col-md-6 text-center">
                    <?php echo wp_get_attachment_image( $twc_image, $size ); ?>
                </div>    
            </div>
        </div>  
    </div>   
</section>
<section class="purchasing">
            <?php
            if( have_rows('rp_items_pr') ):
                $count = 1;
                while( have_rows('rp_items_pr') ) : the_row();
                    $pr_name = get_sub_field('rp_title_pr');
                    $pr_desc = get_sub_field('rp_description_pr');
                    $pr_image = get_sub_field('rp_image_pr');
                    $size = 'full'; ?>
                    <div class="flex-container purchasing-item">
                        <div class="half-width <?php if($count % 2 === 0): echo 'pr-info pr-bg-left'; else: echo "pr-image"; endif; ?>">
                            <?php if($count % 2 === 0): ?>
                            <div class="pr-title-desc">
                                <h2 class="pr-title"><?= $pr_name; ?></h2>
                                <p class="pr-desc"><?= $pr_desc; ?></p>
                            </div>
                            <?php else:
                            echo wp_get_attachment_image( $pr_image, $size ); ?>
                            <?php endif; ?>
                        </div>
                        <div class="half-width <?php if($count % 2 === 0): echo 'pr-image'; else: echo "pr-info pr-bg-right"; endif; ?>">
                            <?php if($count % 2 === 0):
                            echo wp_get_attachment_image( $pr_image, $size );
                            else: ?>
                            <div class="pr-title-desc">
                                <h2 class="pr-title"><?= $pr_name; ?></h2>
                                <p class="pr-desc"><?= $pr_desc; ?></p>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php $count ++; ?>
                <?php endwhile;
            else :
            endif; ?>
</section>
<?php get_template_part("templates/request"); ?>
<?php get_footer(); ?>