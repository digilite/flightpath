<?php 
    $request_bg = get_field('request_section_background', 'option');
?>
<section class="book-request" style="background: url('<?= $request_bg; ?>') center center / cover no-repeat;">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="select_box choose-view">
                    <select class="bold-title view-book">
                        <option style="background-color:#151f28; border: none;" data-content="request_estimate" value="request_estimate">Request An Instant Estimate</option>
                        <option style="background-color:#151f28; border: none;" data-content="join_today" value="join_today">Join Today</option>
                        <option style="background-color:#151f28; border: none;" data-content="get_in_touch" value="get_in_touch">Get In Touch</option>
                    </select>
                </div>
                <div class="request_estimate frm_fly">
                    <div id="fyEQAFrameWrapper" class="fyEQAFrameWrapper">
                        <style type="text/css" scoped>
                            .fyEQAFrameWrapper .fyEQAFrame {
                                width: 100% !important;
                            }
                        </style><iframe id="fyEQAFrame" class="fyEQAFrame"
                            src="https://flyeasy.co/opapi/-fe3-dt2-ht1-ctp:flyflightpath2:-/54b7e593e636e96f3af16ce1/"
                            style="width: 800px; height: 600px; border: none;"></iframe>
                    </div>
                    <script src="https://flyeasy.co/js/loaders/all.api.loader.min.js"></script>
                    <script>initEQA({ autosize: true, frameId: 'fyEQAFrame', id: '54b7e593e636e96f3af16ce1', gaCrossDomainTracking: 'UA-102568840-2' });</script>
                </div>
                <div class="join_today frm_fly">
                    <?php echo do_shortcode('[gravityform id="5"]'); ?>
                </div>
                <div class="get_in_touch frm_fly">
                    <?php echo do_shortcode('[gravityform id="6"]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>