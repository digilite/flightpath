<?php $term_jet = get_queried_object();  ?>
<div class="col-md-3 col-sm-6 col-12">
    <div class="post-item" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID(),'medium_large'); ?>') center center / cover no-repeat">
        <a href="<?php the_permalink(); ?>" class="post-item-url">
            <div class="post-icon">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect x="0.5" y="0.5" width="23" height="22.9995" stroke="white"/>
                    <path d="M9.21875 14.7783L14.7743 9.22281" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M9.21875 9.22281H14.7743V14.7783" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
            <div class="post-info">
                <p class="air-type"><?php echo $term_jet->name; ?></p>
                <p class="post-meta"><?php echo get_the_date(); ?></p>
                <h4 class="post-title bold-title"><?php the_title(); ?></h4>       
            </div>
        </a>
    </div>
</div>