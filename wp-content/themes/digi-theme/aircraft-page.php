<?php get_header(); 
/**
* Template Name: Aircraft Managment
*
* @package WordPress
* @subpackage Digilite Theme
*/
?>
<?php
    $hero_title = get_field('hero_title');
    $hero_description = get_field('hero_description');
    $twc_desc = get_field('twc_description');
    $twc_image = get_field('twc_image');
    $size = 'full';
    $ben_title = get_field('ben_title');
    $ben_bg = get_field("ben_background");
?>
<section class="home-hero" style="background: linear-gradient(180deg, #131E29 9.57%, rgba(19, 30, 41, 0.36) 44.21%, #131E29 100%), url('<?php echo get_the_post_thumbnail_url(); ?>') center center / cover no-repeat;">
    <div class="container">
        <p class="page-title bold-title"><?php the_title(); ?></p>
        <div class="row">
            <div class="hero-info col-md-6">
                <h1 class="general-title"><?= $hero_title; ?></h1>
                <p class="hero-info-description"><?= $hero_description; ?></p>
            </div>
        </div>     
    </div>
    <div class="twc_image_text">
        <div class="container">
            <dic class="row">
                <div class="col-md-5 twc_ds">
                    <?= $twc_desc; ?>
                </div>
                <div class="col-md-7">
                    <?php echo wp_get_attachment_image( $twc_image, $size ); ?>
                </div>    
            </div>
        </div>
     </div>
</section>
<section class="the_benefits" style="background: url('<?= $ben_bg; ?>') center left / contain no-repeat;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="benefits_title"><?= $ben_title; ?></h2>
            </div>
            <div class="col-md-6">
                <div class="accordion" id="accordionExample">
                    <?php
                    if( have_rows('ben_benefits_items') ):
                        $count = 1;
                        while( have_rows('ben_benefits_items') ) : the_row();
                            
                            $ben_item_title = get_sub_field('ben_item_title');
                            $ben_item_desc = get_sub_field('ben_item_description'); ?>
                            <div class="card">
                                <div class="card-header" id="heading<?= $count; ?>">
                                <h5 class="mb-0 acc-title">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?= $count; ?>" aria-expanded="<?php if($count == 1): echo "true"; endif; ?>" aria-controls="collapseOne">
                                    <?= $ben_item_title; ?>
                                    </button>
                                </h5>
                                </div>
                                <div id="collapse<?= $count; ?>" class="collapse <?php if($count == 1): echo "show"; endif; ?>" aria-labelledby="heading<?= $count; ?>" data-parent="#accordionExample">
                                <div class="card-body">
                                    <?= $ben_item_desc; ?>
                                </div>
                                </div>
                            </div>
                    <?php
                    $count ++;
                    endwhile;

                    else :
                    endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_template_part("templates/request"); ?>
<?php get_footer(); ?>