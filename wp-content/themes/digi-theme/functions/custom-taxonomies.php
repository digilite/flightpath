<?php
// ADD CUSTOM TAXONOMY IF NEEDED

function custom_taxonomy_type() {
	$labels = [
		"name"              => _x("Airplane Type", "Airplane Type"),
		"singular_name"     => _x("Airplane Type", "airplane_type"),
		"search_items"      => __("Search Airplane Types"),
		"all_items"         => __("All Airplane Types"),
		"parent_item"       => __("Parent Airplane Type"),
		"parent_item_colon" => __("Parent Airplane Type:"),
		"edit_item"         => __("Edit Airplane Type"),
		"update_item"       => __("Update Airplane Type"),
		"add_new_item"      => __("Add New Airplane Type"),
		"new_item_name"     => __("New Airplane Type"),
		"menu_name"         => __("Airplane Type"),
	];

	$args = [
		"hierarchical"      => true,
		"labels"            => $labels,
		"show_ui"           => true,
		"show_admin_column" => true,
		"query_var"         => true,
		"rewrite"           => ["slug" => "airplane_type"],
	];

	register_taxonomy("airplane_type", ["fleet"], $args);
}
add_action("init", "custom_taxonomy_type", 0);
