<?php get_header(); 
/**
* Template Name: Contact
*
* @package WordPress
* @subpackage Digilite Theme
*/
?>
<?php
    $hero_title = get_field('hero_title');
    $hero_description = get_field('hero_description');
    $hero_subtitle = get_field('hero_subtitle');
?>
<div class="wrapper" style="background: linear-gradient(180deg, #131E29 0%, rgba(19, 30, 41, 0.78) 50%, #131E29 96.72%), url('<?php echo get_the_post_thumbnail_url(); ?>') top center / 100% no-repeat, #131E29;">
<section class="home-hero">
    <div class="container">
        <p class="page-title bold-title"><?php the_title(); ?></p>
        <div class="row">
            <div class="hero-info col-md-5">
                <h1 class="general-title"><?= $hero_title; ?></h1>
                <p class="hero-info-description"><?= $hero_description; ?></p>
            </div>
        </div>
        <p class="subtitle bold-title"><?= $hero_subtitle; ?></p>    
    </div>
</section>
<section class="cities">
    <div class="container">
        <div class="row">
            <?php
            if( have_rows('rp_cities_items') ):
                $count = 1;
                while( have_rows('rp_cities_items') ) : the_row();
                    $city_name = get_sub_field('rp_city_name');
                    $city_desc = get_sub_field('rp_city_description');
                    $city_address = get_sub_field('rp_city_address');
                    $city_phone = get_sub_field('rp_city_phone');
                    $city_image = get_sub_field('rp_city_image');
                    $clear_phone = str_replace('-', '', $city_phone);
                    $size = 'full'; ?>
                    <div class="purchasing-item flex-container">
                        <div class="col-md-6 <?php if($count % 2 === 0): echo 'ct-image'; else: echo "ct-info"; endif; ?>">
                            <?php if($count % 2 === 0):
                            echo wp_get_attachment_image( $city_image, $size );
                            else: ?>
                            <h2 class="city-title"><?= $city_name; ?></h2>
                            <p class="city-desc"><?= $city_desc; ?></p>
                            <p class="city-address bold-title"><?= $city_address; ?></p>
                            <a class="city-phone bold-title" href="tel:<?= $clear_phone; ?>"><?= $city_phone; ?></a>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6 <?php if($count % 2 === 0): echo 'ct-info'; else: echo "ct-image"; endif; ?>">
                            <?php if($count % 2 === 0): ?>
                            <h2 class="city-title"><?= $city_name; ?></h2>
                            <p class="city-desc"><?= $city_desc; ?></p>
                            <p class="city-address bold-title"><?= $city_address; ?></p>
                            <a class="city-phone bold-title" href="tel:<?= $clear_phone; ?>"><?= $city_phone; ?></a>
                            <?php else:
                            echo wp_get_attachment_image( $city_image, $size ); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php $count ++; ?>
                <?php endwhile;
            else :
            endif; ?>
        </div>
    </div>
</section>
</div>
<?php get_footer(); ?>