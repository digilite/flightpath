<?php get_header(); 
/**
* Template Name: Purchasing
*
* @package WordPress
* @subpackage Digilite Theme
*/
?>
<?php
    $hero_title = get_field('hero_title');
    $hero_description = get_field('hero_description');
    $hero_bt_label = get_field('hero_button_label');
    $hero_bt_url = get_field('hero_button_url');
    $link_url = $hero_bt_url['url'];
?>
<section class="home-hero" style="background: linear-gradient(180deg, #131E29 9.57%, rgba(19, 30, 41, 0.36) 44.21%, #131E29 75.53%), url('<?php echo get_the_post_thumbnail_url(); ?>') top center / cover no-repeat;">
    <div class="container">
        <p class="page-title bold-title"><?php the_title(); ?></p>
        <div class="row">
            <div class="hero-info col-md-6">
                <h1 class="general-title"><?= $hero_title; ?></h1>
                <p class="hero-info-description"><?= $hero_description; ?></p>
                <a href="<?php echo esc_url( $link_url ); ?>" class="fill-button-gold"><?= $hero_bt_label; ?></a>
            </div>
        </div>
        <p class="subtitle bold-title"><?= $hero_subtitle; ?></p>    
    </div>
</section>
<section class="purchasing">
            <?php
            if( have_rows('rp_items_pr') ):
                $count = 1;
                while( have_rows('rp_items_pr') ) : the_row();
                    $pr_name = get_sub_field('rp_title_pr');
                    $pr_desc = get_sub_field('rp_description_pr');
                    $pr_image = get_sub_field('rp_image_pr');
                    $size = 'full'; ?>
                    <div class="purchasing-item flex-container">
                    <div class="half-width <?php if($count % 2 === 0): echo 'pr-info pr-bg-left'; else: echo "pr-image"; endif; ?>">
                            <?php if($count % 2 === 0): ?>
                            <div class="pr-title-desc">
                                <h2 class="pr-title"><?= $pr_name; ?></h2>
                                <p class="pr-desc"><?= $pr_desc; ?></p>
                            </div>
                            <?php else:
                            echo wp_get_attachment_image( $pr_image, $size ); ?>
                            <?php endif; ?>
                    </div>
                    <div class="half-width <?php if($count % 2 === 0): echo 'pr-image'; else: echo "pr-info pr-bg-right"; endif; ?>">
                            <?php if($count % 2 === 0):
                            echo wp_get_attachment_image( $pr_image, $size );
                            else: ?>
                        <div class="pr-title-desc">
                            <h2 class="pr-title"><?= $pr_name; ?></h2>
                            <p class="pr-desc"><?= $pr_desc; ?></p>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php $count ++; ?>
            <?php endwhile;
        else :
        endif; ?>
</section>
<?php get_template_part("templates/request"); ?>
<?php get_footer(); ?>