<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<!-- Generate favicon here http://www.favicon-generator.org/ -->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php
	$logo = get_field('header_logo', 'option');
	$size = "full";
	$header_bt_label = get_field('header_button_label', 'option');
	$header_bt_url = get_field('header_button_url', 'option');
	$ph_number = get_field('phone_number', 'option');
    $clear_phone = str_replace('-', '', $ph_number);
	?>
	<div class="mobile-menu">
		<div class="call_to_action">
			<a class="request button-gold" href="<?= $header_bt_url; ?>"><?= $header_bt_label; ?></a>
		</div>
		<div class="mobile-nav" itemscope itemtype="http://schema.org/SiteNavigationElement"><?php
				wp_nav_menu([
					'theme_location' => 'primary-menu',
					'menu_class' => 'mobile-main-menu',
					'container' => '',
				]); ?>
		</div>
		<div class="row justify-content-between footer-logos">
					<div class="footer-social col-md-6 col-4">
						<?php get_template_part("social"); ?>
					</div>
					<div class="ft-logos flex-container justify-content-end col-md-6 col-8">
						<?php
						if( have_rows('footer_logos', 'options') ):
							while( have_rows('footer_logos',  'options') ) : the_row();
								$logo1 = get_sub_field('ft_logo');
								$size = 'full'; 
								if( $logo1 ) {
									echo wp_get_attachment_image( $logo1, $size );
								}
								endwhile;
								else :
								endif; ?>
					</div>
		</div>
	</div>
	<header itemscope itemtype="http://schema.org/WPHeader">
		<div class="container header-items">
			<div class="row header-row">
				<div class="phone-number col-md-4">
					<a href="tel:<?= $clear_phone; ?>"><?= $ph_number; ?></a>
				</div>
				<div class="col-md-4 col-4 text-center logo" itemscope itemtype="http://schema.org/Organization" id="logo">
					<a itemprop="url" href="<?php echo bloginfo('url') ?>">
						<?php if( $logo ) {
							echo wp_get_attachment_image( $logo, $size );
						} ?>
					</a>
				</div>
				<div class="call_to_action col-md-4 text-right">
					<a class="request button-gold" href="<?= $header_bt_url; ?>"><?= $header_bt_label; ?></a>
				</div>
				<div id="burger-icon" class="col-md-4">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
		</div>
		<nav itemscope itemtype="http://schema.org/SiteNavigationElement"><?php
			wp_nav_menu([
				'theme_location' => 'primary-menu',
				'menu_class' => 'main-menu',
				'container' => '',
			]); ?>
		</nav>
	</header>